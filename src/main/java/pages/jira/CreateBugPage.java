package pages.jira;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.pages.JiraConstants.*;

public class CreateBugPage extends BaseJiraPage{

    public CreateBugPage (WebDriver webDriver){
        super(webDriver,"jira.project.page");
    }

    public void assertInCorrectProject() {
        actions.waitForElementVisible("jira.projectPage.projectName");
        actions.assertElementPresent("jira.projectPage.projectName");
    }

    public void clickOnCreateButton(){
        actions.waitForElementVisibleUntilTimeout("jira.projectPage.createButton",30);
        actions.clickElement("jira.projectPage.createButton");

    }

    public void issueTypeDropDownButton(){
        actions.waitForElementVisible("jira.issuePage.issueTypeButton");
        actions.clickElement("jira.issuePage.issueTypeButton");
        actions.clickElement("jira.issuePage.issueTypeButton.selectBug");
    }
    public void addSummary(){
        actions.waitForElementVisible("jira.issuePage.addSummary");
        actions.typeValueInField(BUG_SUMMARY,"jira.issuePage.addSummary");
    }

    public void addDescription(){
        actions.waitForElementVisible("jira.issuePage.addDescription");
        actions.typeValueInField(BUG_DESCRIPTION,"jira.issuePage.addDescription");
    }

    public void selectPriority(){
        actions.waitForElementVisible("jira.issuePage.selectPriority");
        actions.clickElement("jira.issuePage.selectPriority");
        actions.clickElement("jira.issuePage.priorityOptionsBug");

    }
        public void typeEnvironment(){
            actions.waitForElementVisible("jira.issuePage.environmentBug");
            actions.typeValueInField(BUG_ENVIRONMENT,"jira.issuePage.environmentBug");
        }


    public void selectAssignee(){
        actions.waitForElementVisible("jira.issuePage.assigneeButton");
        actions.clickElement("jira.issuePage.assigneeButton");
    }

    public void clickCreateIssueButton(){
        actions.waitForElementVisible("jira.issuePage.createIssueButton");
        actions.clickElement("jira.issuePage.createIssueButton");
    }

    public void assertBugAddedToTheBackLog(){
        actions.waitForElementVisible("jira.projectPage.issueAddedToTheBackLog");
    }




}
