package pages.jira;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;


public class HomePage extends BasePage {

    public HomePage(WebDriver webDriver){
        super(webDriver,"jira.homepage.url");
    }
    public void myAccountDropDownButton(){
        actions.clickElement("jira.homepage.myAccountButton");
    }
    public void logInButton(){
        actions.clickElement("jira.homepage.loginButton");

    }
    public void assertNavigatedToHomePage(){
        actions.assertNavigatedUrl("jira.homepage.url");

    }



}
