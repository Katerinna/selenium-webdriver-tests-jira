package pages.jira;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BaseJiraPage {

    public LoginPage(WebDriver webDriver){
        super(webDriver,"jira.loginUrl");

    }

    public void loginUser(String userKey){
        String username = Utils.getConfigPropertyByKey("jira.users." + userKey + ".username");
        String password = Utils.getConfigPropertyByKey("jira.users." + userKey + ".password");

        navigateToPage();

        actions.waitForElementVisible("jira.loginPage.username");
        actions.typeValueInField(username, "jira.loginPage.username");
        actions.clickElement("jira.loginPage.continueButton");

        actions.waitFor(2000);

        actions.waitForElementVisible("jira.loginPage.password");
        actions.typeValueInField(password, "jira.loginPage.password");
        actions.clickElement("jira.loginPage.loginButton");
    }



    public void assertAvatarIsPresent(){
        actions.waitForElementVisible("jira.loginPage.avatar");
        actions.assertElementPresent("jira.loginPage.avatar");
    }

    public void assertNavigatedToProjectsPage(){
        actions.assertNavigatedUrl("jira.projects.page");
    }

    public void loadProject(){
        actions.waitForElementVisible("jira.loginPage.project");
        actions.clickElement("jira.loginPage.project");


    }
}




