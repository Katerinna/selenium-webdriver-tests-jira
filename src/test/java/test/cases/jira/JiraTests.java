package test.cases.jira;

import org.junit.Test;
import pages.jira.CreateBugPage;
import pages.jira.CreateStoryPage;
import pages.jira.HomePage;
import pages.jira.LoginPage;

import static com.telerikacademy.testframework.pages.JiraConstants.BUG_SUMMARY;
import static com.telerikacademy.testframework.pages.JiraConstants.STORY_SUMMARY;

public class JiraTests extends BaseTest{
    @Test
         public void login() {
        HomePage homePage = new HomePage(actions.getDriver());
        homePage.navigateToPage();

        homePage.assertNavigatedToHomePage();

        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("user");

        loginPage.assertAvatarIsPresent();
        loginPage.assertNavigatedToProjectsPage();
    }

    @Test
    public void createStory(){
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("user");

        loginPage.loadProject();


        CreateStoryPage createStoryPage = new CreateStoryPage(actions.getDriver());
        createStoryPage.assertInCorrectProject();


        createStoryPage.clickOnCreateButton();
        createStoryPage.issueTypeDropDownButton();
        createStoryPage.addSummary();
        createStoryPage.addDescription();
        createStoryPage.selectAssignee();
        createStoryPage.selectPriority();
        createStoryPage.clickCreateIssueButton();

        createStoryPage.assertStoryAddedToTheBackLog();
    }

    @Test

    public void createBug(){
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("user");

        loginPage.loadProject();


        CreateBugPage createBugPage = new CreateBugPage(actions.getDriver());
        createBugPage.assertInCorrectProject();
        createBugPage.clickOnCreateButton();
        createBugPage.issueTypeDropDownButton();
        createBugPage.addSummary();
        createBugPage.addDescription();
        createBugPage.selectPriority();
        createBugPage.typeEnvironment();
        createBugPage.selectAssignee();
        createBugPage.clickCreateIssueButton();

        createBugPage.assertBugAddedToTheBackLog();
    }


}
